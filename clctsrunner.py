#!/usr/bin/env python3
import argparse
import itertools
import json
import multiprocessing
import os
from pathlib import Path
import re
import subprocess
import sys
import time
from tqdm import tqdm

os.sync()
CTS_PATH=Path('/home/kherbst/git/OpenCL-CTS/build/test_conformance')
TIMEOUT=3600

passing = [
	'allocations',
	'api',
	'atomics',
	'basic',
# 2.0	'c11_atomics',
# RA	'commonfns',
# fails	'compiler',
	'computeinfo',
# RA	'contractions',
	'device_execution',
	'device_partition',
	'device_timer',
	'events',
	'select',
	'vec_step',
]

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dry-run', action='store_true')
parser.add_argument('-f', '--test-file', action='append', type=str, default=[])
parser.add_argument('-i', '--include', action='append', type=str, default=[])
parser.add_argument('-j', '--jobs', type=int, default=multiprocessing.cpu_count())
parser.add_argument('-s', '--singlethreaded', action='store_true')
parser.add_argument('-v', '--version', type=str, default='1.2')
parser.add_argument('-w', '--wimpy', action='store_true')
parser.add_argument('-x', '--exclude', action='append', type=str, default=['buffers'])
args = parser.parse_args()

os.environ['NV50_PROG_DEBUG'] = '0'
if len(args.version):
	os.environ['CLOVER_DEVICE_VERSION_OVERRIDE'] = args.version
	os.environ['CLOVER_DEVICE_CLC_VERSION_OVERRIDE'] = args.version
	os.environ['CLOVER_PLATFORM_VERSION_OVERRIDE'] = args.version
os.environ['NOUVEAU_ENABLE_CL'] = '1'

os.environ['CL_TEST_SINGLE_THREADED'] = '1'
if args.wimpy:
	os.environ['CL_WIMPY_MODE'] = '1'

if len(args.include) == 0:
	args.include = passing
elif args.include[0] == 'all':
	args.include = []

class DumpAsJson:
	def __str__(self):
		return json.dumps(self, cls=TestEncoder)

	def __repr__(self):
		return str(self)

class TestResults(DumpAsJson):
	def __init__(self):
		self.crashes = []
		self.fails = []
		self.passes = []
		self.timeouts = []

result = TestResults()

class TestResult(DumpAsJson):
	def __init__(self, returncode, stdout, stderr, id, name, timedout):
		self.id = id
		self.name = name
		self.returncode = returncode
		self.timedout = timedout
		self.stdout = stdout
		self.stderr = stderr

	def __lt__(self, other):
		return self.id + ' ' + self.name < other.id + ' ' + other.name

class TestSuite(DumpAsJson):
	__regex_def = re.compile('^\s*Test names.*:$\n', re.M)
	__regex_math = re.compile('^Math function names:$\n', re.M)
	__regex_printf = re.compile('\s*default is to run the full test on the default device')

	__procs = []

	def __init__(self, id, path, tests):
		self.id = id
		self.path = Path(path)
		assert(len(tests) > 0)
		self.tests = tests

	def execute(self, test):
		if args.dry_run:
			return TestResult(0, '', '', self.id, test, False)

		timedout = False
		add_args = []
		if self.id == 'spirv':
			add_args = ['--spirv-binaries-path', '%s/../../test_conformance/spirv_new/spirv_bin/' % CTS_PATH]

		try:
			proc = subprocess.Popen([self.path, test] + add_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
			TestSuite.__procs.append(proc)
			proc.wait(timeout=TIMEOUT)
		except subprocess.TimeoutExpired:
			proc.kill()
			timedout = True
		except:
			print('Unexpected error:', sys.exc_info()[0])

		TestSuite.__procs.remove(proc)
		stdout, stderr = proc.communicate()
		return TestResult(proc.returncode, stdout, stderr, self.id, test, timedout)

	@classmethod
	def create(cls, id, file):
		path = CTS_PATH.joinpath(file)
		assert(path.exists())

		# we have to create the list ourselves
		if id == 'conversions':
			params = [
				['char', 'uchar', 'short', 'ushort', 'int', 'uint', 'long', 'ulong', 'float', 'double'], #dest
				['', '_sat'], #sat
				['', '_rte', '_rtp', '_rtn', '_rtz'], #mode
				['char', 'uchar', 'short', 'ushort', 'int', 'uint', 'long', 'ulong', 'float', 'double'], #src
			]
			combinations = map(lambda t: '%s%s%s_%s' % t, itertools.product(*params))
			return TestSuite(id, path, list(combinations))

		args = [path]
		if id == 'math_brute_force':
			args.append('-p')
			regex = cls.__regex_math
		elif id == 'printf':
			args.append('-h')
			regex = cls.__regex_printf
		else:
			args.append('-h')
			regex = cls.__regex_def

		res = subprocess.run(args, capture_output=True)
		output = res.stdout.decode()
		match = re.search(regex, output)
		assert(match)
		output = output[match.end():]
		return TestSuite(id, path, output.split())

	@classmethod
	def killAll(cls):
		for p in cls.__procs:
			if p.poll() is not None:
				return
			p.terminate()
			p.wait(.01)
			if p.poll() is not None:
				return
			p.kill()

class TestEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, TestResults):
			return obj.__dict__
		elif isinstance(obj, TestResult):
			return obj.__dict__
		elif isinstance(obj, TestSuite):
			return obj.__dict__
		elif isinstance(obj, Path):
			return str(obj)
		return json.JSONEncoder.default(self, obj)


# todo: half
# todo: headers
# todo: spir
testSuites = {k: TestSuite.create(k, v) for k, v in {
	'allocations': 'allocations/test_allocations',
	'api': 'api/test_api',
	'atomics': 'atomics/test_atomics',
	'basic': 'basic/test_basic',
	'c11_atomics': 'c11_atomics/test_c11_atomics',
	'commonfns': 'commonfns/test_commonfns',
	'compiler': 'compiler/test_compiler',
	'computeinfo': 'computeinfo/test_computeinfo',
	'contractions': 'contractions/test_contractions',
	'conversions': 'conversions/test_conversions',
	'device_execution': 'device_execution/test_device_execution',
	'device_partition': 'device_partition/test_device_partition',
	'device_timer': 'device_timer/test_device_timer',
	'events': 'events/test_events',
	'generic_address_space': 'generic_address_space/test_generic_address_space',
	'geometrics': 'geometrics/test_geometrics',
	'images_cl_fill_images': 'images/clFillImage/test_cl_fill_images',
	'images_samplerless_reads': 'images/samplerlessReads/test_samplerless_reads',
	'images_kernel_image_methods': 'images/kernel_image_methods/test_kernel_image_methods',
	'images_cl_get_info': 'images/clGetInfo/test_cl_get_info',
	'images_cl_copy_images': 'images/clCopyImage/test_cl_copy_images',
	'images_image_streams': 'images/kernel_read_write/test_image_streams',
	'images_cl_read_write_images': 'images/clReadWriteImage/test_cl_read_write_images',
	'integer_ops': 'integer_ops/test_integer_ops',
	'math_brute_force': 'math_brute_force/test_bruteforce',
	'mem_host_flags': 'mem_host_flags/test_mem_host_flags',
	'multiple_device_context': 'multiple_device_context/test_multiples',
	'non_uniform_work_group': 'non_uniform_work_group/test_non_uniform_work_group',
	'pipes': 'pipes/test_pipes',
	'printf': 'printf/test_printf',
	'profiling': 'profiling/test_profiling',
	'relationals': 'relationals/test_relationals',
	'select': 'select/test_select',
	'spirv': 'spirv_new/test_spirv_new',
	'subgroups': 'subgroups/test_subgroups',
	'SVM': 'SVM/test_svm',
	'thread_dimensions': 'thread_dimensions/test_thread_dimensions',
	'vec_align': 'vec_align/test_vecalign',
	'vec_step': 'vec_step/test_vecstep',
	'workgroups': 'workgroups/test_workgroups',
}.items()}

disabled = {
	'buffers': 'buffers/test_buffers',
	'clcpp_synchronization': 'clcpp/synchronization/test_cpp_synchronization',
	'clcpp_geometric_funcs': 'clcpp/geometric_funcs/test_cpp_geometric_funcs',
	'clcpp_address_spaces': 'clcpp/address_spaces/test_cpp_address_spaces',
	'clcpp_convert': 'clcpp/convert/test_cpp_convert',
	'clcpp_api': 'clcpp/api/test_cpp_api',
	'clcpp_atomics': 'clcpp/atomics/test_cpp_atomics',
	'clcpp_workitems': 'clcpp/workitems/test_cpp_workitems',
	'clcpp_images': 'clcpp/images/test_cpp_images',
	'clcpp_vload_vstore_funcs': 'clcpp/vload_vstore/test_cpp_vload_vstore_funcs',
	'clcpp_pipes': 'clcpp/pipes/test_cpp_pipes',
	'clcpp_workgroups': 'clcpp/workgroups/test_cpp_workgroups',
	'clcpp_device_queue': 'clcpp/device_queue/test_cpp_device_queue',
	'clcpp_relational_funcs': 'clcpp/relational_funcs/test_cpp_relational_funcs',
	'clcpp_common_funcs': 'clcpp/common_funcs/test_cpp_common_funcs',
	'clcpp_attributes': 'clcpp/attributes/test_cpp_attributes',
	'clcpp_subgroups': 'clcpp/subgroups/test_cpp_subgroups',
	'clcpp_reinterpret': 'clcpp/reinterpret/test_cpp_reinterpret',
	'clcpp_integer_funcs': 'clcpp/integer_funcs/test_cpp_integer_funcs',
	'clcpp_program_scope_ctors_dtors': 'clcpp/program_scope_ctors_dtors/test_cpp_program_scope_ctors_dtors',
	'clcpp_spec_constants': 'clcpp/spec_constants/test_cpp_spec_constants',
	'clcpp_math_funcs': 'clcpp/math_funcs/test_cpp_math_funcs',
}

desc_str = 'Pass %u Fails %u Crashes %u Timeouts %u'

def update_str():
	return desc_str % (len(result.passes), len(result.fails), len(result.crashes), len(result.timeouts))

def update_desc():
	pbar.set_description(update_str())

def update(res):
	if res.timedout:
		result.timeouts.append(res)
	elif res.returncode == 0:
		result.passes.append(res)
	elif res.returncode in [-6, -11]:
		result.crashes.append(res)
	else:
		result.fails.append(res)
	update_desc()
	pbar.update()

def poolError(*err):
	print('pool error')
	print(err)

includes = set(args.include)
excludes = set(args.exclude)

def filter(t):
	res = [(t, test) for test in t.tests]
	if len(includes) > 0:
		return res if t.id in includes else []
	else:
		return res if t.id not in excludes else []

tests = []
if (len(args.test_file) > 0):
	for f in args.test_file:
		path = Path(f)
		content = path.read_text('utf-8')
		for line in content.splitlines():
			tuple = line.split(' ')
			tests.append((testSuites[tuple[0]], tuple[1]))
else:
	tests = list(itertools.chain.from_iterable(map(filter, testSuites.values())))

pool = multiprocessing.Pool(1 if args.singlethreaded else args.jobs)
pbar = tqdm(total=len(tests))
update_desc()

for t in tests:
	pool.apply_async(TestSuite.execute, t, callback=update, error_callback=poolError)
pool.close()

try:
	pool.join()
except (KeyboardInterrupt, SystemExit):
	None
finally:
	TestSuite.killAll()
pbar.close()

print(result)
print('\npasses:')
for tr in sorted(result.passes):
	print('%s %s' % (tr.id, tr.name))

print('\nfails:')
for tr in sorted(result.fails):
	print('%s %s' % (tr.id, tr.name))

print('\ncrashes:')
for tr in sorted(result.crashes):
	print('%s %s' % (tr.id, tr.name))

print('\ntimeouts:')
for tr in sorted(result.timeouts):
	print('%s %s' % (tr.id, tr.name))

print('\n' + update_str())
